#include <iostream>


class vector
{
private:
    double X, Y, Z;
public:
    vector() : X(1), Y(1), Z(1) {}
    vector(double x, double y, double z): X(x), Y(y), Z(z) {}
    double GetX() { return X; }
    void SetX(const double& x) { X = x; }
    double GetY() { return Y; }
    void SetY(const double& y) { Y = y; }
    double GetZ() { return Z; }
    void SetZ(const double& z) { Z = z; }
    double GetMod() { return sqrt(X*X+Y*Y+Z*Z); }
    void Show()
    {
        std::cout << "(" << X << ", " << Y <<  ", " << Z << ")\n";
    }
};

int main()
{
    vector kek(2, 5, 1);
    kek.SetY(2);
    kek.Show();
    std::cout << kek.GetMod() << "\n";
}
